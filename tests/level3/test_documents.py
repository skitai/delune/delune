import delune
import os
import skitai
import atila
from delune.cli import local
import shutil
from delune.fields import *
import glob
from delune.cli import indexer
import time
from delune.searcher.searcher import Searcher

Searcher.MAINTERN_INTERVAL = 1

def _index (api, cname, naics):
    for idx, (code, title) in enumerate (sorted (list (naics.items ())) [:10]):
        d = delune.document (code)
        d.add_content ({"id": code, "title": title, 'rownum': idx})
        d.set_snippet (title, option = "strip_hrml")
        d.add_field ("default", "$content.title", TEXT)
        d.add_field ("code", "$content-0.id", STRING)
        r = api.cols (cname).documents (code).put (d.as_dict ())
        assert r.status_code == 202

def test_documents (app, rdir, cname, naics):
    with app.test_client () as cli:
        r = cli.get ("/status")
        assert r.status_code == 200

        api = cli.api ("/")

        config = local.make_config (cname, [cname])
        r = api.cols (cname).post (config, side_effect = "now")
        assert r.status_code == 201

        r = api.cols (cname).patch ( {"section": "analyzer", "data": {"stem_level": 2, "make_lower_case": True} })
        assert r.status_code == 200
        assert r.data ["analyzer"]["stem_level"] == 2

        _index (api, cname, naics)
        qfile = glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.lock"))
        assert qfile

        r = api.cols (cname).rollback.get ()
        assert r.status_code == 405
        qfile = glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.lock"))
        assert qfile

        r = api.cols (cname).rollback.post ({})
        assert r.status_code == 205
        assert qfile != glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.lock"))

        _index (api, cname, naics)
        r = api.cols (cname).commit.post ({})
        assert r.status_code == 205
        assert not glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.lock"))
        assert glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.*"))

        _index (api, cname, naics)
        r = api.cols (cname).commit.post ({})
        assert r.status_code == 205
        assert len (glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.*"))) == 2
        indexer.index (rdir)
        assert len (glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.*"))) == 0

        r = api.cols (cname).get ()
        st = r.data
        assert len (st ["segmentinfos"]) == 1
        assert len (st ["segmentfiles"]["primary"]["0"]) == 7

        _index (api, cname, naics)
        r = api.cols (cname).commit.post ({})
        assert r.status_code == 205
        assert len (glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.*"))) == 1
        indexer.index (rdir)
        assert len (glob.glob (os.path.join (rdir, "delune", "collections", cname, ".que", "*.*"))) == 0
        assert len (st ["segmentfiles"]["primary"]) == 2

        #---------------------------------------------------
        dln = delune.mount (rdir)
        col = dln.load (cname)
        with col.documents as d:
            r = d.search ("farming")
            assert r ["total"] == 8
        col.close ()

    with app.test_client () as cli:
        api = cli.api ("/")

        time.sleep (2) # maintern time
        r = api.cols (cname).documents.get (q = "farming")
        assert r.headers.get ('etag')
        assert r.data ["total"] == 8

        r = api.cols (cname).documents.get (q = "farming")
        assert r.headers.get ('etag')
        assert r.data ["msg"] == "Hit Cache"

        ETAG = r.headers.get ('etag')
        r = api.cols (cname).documents.get (q = "farming", headers = { "If-None-Match": ETAG })
        assert r.status_code == 304
        assert r.headers.get ('etag')

        r = api.cols (cname).documents.put ({"q": ["farming", "service"]})
        assert not r.headers.get ('etag')
        assert len (r.data) == 2
        assert 'farming' in r.data
        assert r.data ['farming']["total"] == 8

        _index (api, cname, naics)
        r = api.cols (cname).commit.post ({})
        assert r.status_code == 205
        indexer.index (rdir)

        time.sleep (3)
        r = api.cols (cname).documents.get (q = "farming", headers = { "If-None-Match": ETAG })
        assert r.status_code == 200
        assert r.headers.get ('etag')

        r = api.cols (cname).delete (side_effect = "data")
        assert not r.headers.get ('etag')
        assert r.status_code == 202

        time.sleep (2)
        r = api.cols ("pytest-temp").get ()
        assert not r.headers.get ('etag')
        assert r.status_code == 404
